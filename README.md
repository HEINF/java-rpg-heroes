# Java_RPG_Characters
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

# Introduction
This project was made in order to learn the basics of Java and Junit testing. It was initially created on github: https://github.com/HeinF/rpg-heroes but moved to GitLab as the instructions for setting up a CI/CD pipeline with test reports where better described for GitLab.

## Project settings
This project has been made with gradle as the build tool and uses JDK 17.

## Install
Clone project into IntelliJ or your choice of compatible editor.

## Usage
The projects purpose is for Unit testing, therefore the actual program only displays a short greeting message. 
Unit tests can be run though, and they are set up to automatically run in the CI/CD pipeline.

## Test reports
Test reports are taken from the CI/CD build job artefact and can be found in the folder 'test reports'.

## Contributing
Not open for contributions.

## License
Unknown
